const express = require('express');
const bodyParser = require('body-parser');
const port = 8000;

const contatosRouter = require('./Routes/contatos');
const usuariosRouter = require('./Routes/usuarios');

let app = express();
app.use(bodyParser.json());

app.use('/contatos',contatosRouter);
app.use('/usuarios',usuariosRouter);

app.listen(port,()=>{
    console.log("Projeto executando na porta " + port);
});