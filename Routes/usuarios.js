const express = require('express');
const router = express.Router();
const db = require('../db/connection');
const bcrypt = require('bcrypt');

router.post('/',(req,res)=>{
    let obj = req.body;
    let cmd_select = 'SELECT USUARIO FROM USUARIOS WHERE USUARIO = ?';

    db.query(cmd_select,obj.usuario,(err,row)=>{
        if(row.length>0){
            res.status(404).json({message:"Usuário já existe!"});
        }else{
            const senhacriptografada = bcrypt.hashSync(obj.password,10);

            obj.password = senhacriptografada;

            let cmd_insert = 'INSERT INTO USUARIOS SET ?';
            db.query(cmd_insert,obj,(error,result)=>{
                if(error){
                    return res.status(404).json({message:"Erro: " + error});
                }else{
                    return res.status(201).json({message:"Usuário criado!"});
                }
            });
        }
    });
});

router.post('/auth',(req,res)=>{
    const {usuario, password} = req.body;

    if(!usuario||!password)
        return res.status(404).json({message:"Dados insuficientes!"});
    
    cmd_select = "SELECT PASSWORD FROM USUARIOS WHERE USUARIO = ?";
    db.query(cmd_select, usuario,(err,row)=>{
        if(row.length>0){
            const same = bcrypt.compareSync(password,row[0].PASSWORD);
            if(same)
                return res.status(200).json({message:"OK"});
            else
                return res.status(401).json({message:"SENHA NÃO CONFERE"});
        }
    });
});

module.exports = router;