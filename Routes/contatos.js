const express = require('express');
const router = express.Router();
const db = require ('../db/connection');

router.get('/', (req, res)=>{
    let cmd_selectAll = 'SELECT * FROM CONTATO';
    db.query(cmd_selectAll,(err, rows)=>{
        res.status(200).json(rows);
    });
});

router.post('/',(req, res)=>{
    let dados = req.body;
    let cmd_insert = 'INSERT INTO CONTATO SET ?';
    db.query(cmd_insert,dados,(error, result)=>{
        if(error){
            res.status(400).json({message:"Erro: " + error})
        }else{
            res.status(201).json({message: result.insertId + " - Contato salvo!"});
        }
    });
});

router.put('/:id',(req, res)=>{
    let id = req.params.id;
    let obj = req.body;

    let sql = "UPDATE CONTATO SET NOME=?, IDADE=?, EMAIL=? , NUMERO=? WHERE ID=?";
    let values = [obj.nome, obj.idade, obj.email, obj.numero, id];

    db.query(sql,values,(error,result)=>{
        if(error){
            res.status(400).json({message:"Erro: " + error})
        }else{
            res.status(201).json({message: " Contato alterado!"});
        }
    });
});

router.delete('/:id',(req, res)=>{
    let id = req.params.id;
    let cmd_delete = "DELETE FROM CONTATO WHERE ID = ?";

    db.query(cmd_delete,id,(error,result)=>{
        if(error){
            res.status(400).json({message:"Erro: " + error})
        }else{
            res.status(201).json({message: " Contato excluído!"});
        }
    });
});

module.exports = router;